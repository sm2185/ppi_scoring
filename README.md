Calculating PI-score to assess the quality of protein-protein interfaces

PI-score is machine-learning based density independent metric to assess the quality of protein-protein interfaces modelled in the cryo-EM assemblies

Please see [Wiki](https://gitlab.com/sm2185/ppi_scoring/-/wikis/home) for more information on dowload/usage.

