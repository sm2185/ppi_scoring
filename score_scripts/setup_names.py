#Provide the paths for required softwares
'''
Edit this file to set-up local paths
'''

muscle_path = '/d/mt18/u/ms005/soft_bin/muscle3.8.31_i86linux64'
usearch_path = '/d/mt18/u/ms005/soft_bin/usearch11.0.667_i86linux32'
db_path = '/d/mt18/u/ms005/databases/uniprot_sprot.fasta'

psiblast_path = 'psiblast'
rate4site_path = 'rate4site'
blastdbcmd_path = 'blastdbcmd'
pisa_cmd = 'pisa'
sc_cmd = 'sc'