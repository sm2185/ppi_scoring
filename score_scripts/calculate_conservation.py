import os
import shutil
from conservation_utils import get_ch_seq, write_all_ch_seq, filter_homologs, run_r4site,get_pdb_numbering, split_bin
import glob
import subprocess
from multiprocessing import Pool
import re
from Bio.PDB.Polypeptide import one_to_three
import operator
import json
import setup_names

        
def run_pre_rate4site(pdbfile,
                  chain_ids=None):
    '''
    For a given pdbfile does multiple jobs for conservation
    1. gets the fasta sequence of all chains
    2. Splits the fasta of all chains
    3. Runs psiblast to get homologs
    4. filter the seuqneces for atleast 80% qcoverage 
    5. Clusters the homologs at 95% identity
    5. Trims the homologs for domain boundaries that align with input sequence
    6. Builds multiple sequence alignment using muscle
    7. Runs rate4site to get get the rate4site r4s.res output file with ML scores 
    '''
    pdbid = pdbfile.rsplit('.',1)[0]
    outpath = pdbid + '_cons'
    if os.path.isdir(outpath):
        shutil.rmtree(outpath)
    os.mkdir(outpath)
    r4site_jobs = []
    #get chains's sequence
    ch_seq = get_ch_seq(pdbFile=pdbfile,
                        outfile=outpath,
                        chain_ids=chain_ids)
    all_ch_seq = glob.glob(outpath+"/*_ch.fasta")
    for fastafle in all_ch_seq:
        bl_cmd = [setup_names.psiblast_path, '-query' , fastafle , 
                  '-db' , setup_names.db_path , 
                  '-evalue' ,'0.00001' , '-out' , fastafle.rsplit('.',1)[0]+'_blast.out' , 
                  '-num_threads', '24', '-num_iterations' ,'3' , '-max_target_seqs' , '350',
                  '-outfmt' , "6 qseqid sseqid pident length qstart qend sstart send evalue qcovs bitscore"]
        p = subprocess.call(bl_cmd, stdout=subprocess.PIPE)
    
    write_all_ch_seq(outfile_name=outpath+'/'+pdbid+'_full.fasta', 
                     infile_list=all_ch_seq)
    
    #Get the homolog sequences from the blast sequence with dom boundaries
    for blout in glob.glob(outpath+"/*_blast.out"):
        try:
            if os.stat(blout).st_size > 0:
                print 'Getting the homolog sequences from the blast sequence with dom boundaries for', blout
                filter_homologs(blout)
            else:
                print "No homologs found using BLAST for %s: empty file"%(blout)
                continue
        except OSError:
            print "No file: Check BLAST not run properly for %s"%(blout)
            continue
        
    for blseq in glob.glob(outpath +'/*_blast_seqcut.fasta'):
        #Cluster the homologs sequences at 95% and use msa to find conservation using rate4site
        cmd = setup_names.usearch_path + ' -id 0.95 -cluster_fast ' \
            + blseq + ' -centroids ' + blseq.rsplit('_blast_seqcut.fasta',1)[0]+'_blast_rep.fasta'
        os.system(cmd)
        add_query_seq_cmd = ('cat ' + blseq.rsplit('_blast_seqcut.fasta',1)[0]+'_blast_rep.fasta' 
                             +  ' ' + blseq.rsplit('_blast_seqcut.fasta',1)[0] + '.fasta' + ' > ' 
                             + blseq.rsplit('blast_seqcut.fasta',1)[0] + 'blast_selected.fasta')
        os.system(add_query_seq_cmd)
        #Align the homologs using Muscle
        aln_cmd = setup_names.muscle_path + ' -in ' \
                + blseq.rsplit('blast_seqcut.fasta',1)[0] + 'blast_selected.fasta' \
                + ' -out ' + blseq.rsplit('_blast_seqcut.fasta',1)[0] + '_rep_cl.aln' \
                + ' -clwstrict -quiet'
        os.system(aln_cmd)
        #Rate4site
        r4site_cmd = [setup_names.rate4site_path, '-s', os.getcwd() + '/' + blseq.rsplit('_blast_seqcut.fasta',1)[0] 
                      + '_rep_cl.aln', '-o', os.getcwd() + '/' + blseq.rsplit('_blast_seqcut.fasta',1)[0] 
                      + '_r4s.res', '-a', blseq.rsplit('_ch_blast_seqcut.fasta',1)[0].rsplit('/',1)[-1]]
        r4site_jobs.append(r4site_cmd)
    return os.path.join(os.getcwd(),outpath), r4site_jobs

def pool_r4site_jobs(number_of_processes,r4site_jobs):
    pool = Pool(number_of_processes)
    results_cl = pool.map(run_r4site, r4site_jobs)
       
def write_conserved_dict(consdir):
    r4site_res = []
    for fle in os.listdir(consdir):
        if fle.endswith('_r4s.res'):
            r4site_res.append(os.path.join(consdir,fle))
            
    if not r4site_res: 
        return
    conserved_res = {}
    conserved_res1 = {}
    
    for resultsfile in r4site_res:
        try:
            pdb_number = get_pdb_numbering(resultsfile.split('_cons')[0] + '.pdb')
        except:
            pdb_number = get_pdb_numbering(resultsfile.split('_cons')[0] + '_clean.pdb')
    
        pdb_id = consdir.split('_cons')[0]
        cons_score = []
        res_lst = []
        conserved_res = {}
        ct = 0
        ch_id = resultsfile.split('_ch')[0][-1]
        for line in open(resultsfile):
            if not line.startswith('#'):
                if len(line.strip()) != 0 :
                    fields = re.split('\s+', line)
                    c = float(fields[3])
                    cons_score.append(float(fields[3]))
                    res = one_to_three(fields[2]) + str(pdb_number[ch_id][ct])
                    res_lst.append(one_to_three(fields[2]) + str(pdb_number[ch_id][ct]))
                    try:
                        conserved_res[ch_id].update({res:c})
                    except KeyError:
                        conserved_res[ch_id] = {res:c}
                    ct += 1
        for ch_id1 in conserved_res.keys():
            sorted_values = sorted(conserved_res[ch_id1].items(), key=operator.itemgetter(1))
        scores = []
        res= []
        for pair in sorted_values:
            scores.append(pair[1])
            res.append(pair[0])
            
        #Get the bin numbers and save only the 9,8,7 conserved bins
        nbin = 9
        t = []
        for el2 in list(split_bin(sorted(scores), 9))[0:3]:
            scr_index_list = []
            for val in el2:
                if val in t: continue
                scr_index_list = [i for i in range(len(scores)) if scores[i] == val]
                for sc in scr_index_list:
                    corres_res = res[sc]
                    try: conserved_res1[ch_id].append([corres_res,val,nbin])
                    except KeyError: conserved_res1[ch_id] = [[corres_res,val,nbin]]
                t.append(val)
        nbin -=1
    outdict = os.path.join(consdir.rsplit('/',1)[0],pdb_id + '_conserved_residues_dict.json')
         
    with open(outdict,'w') as outfle:
        json.dump(conserved_res1,outfle)
    
    write_conserved_interface_dict(consdir.rsplit('/',1)[0],pdb_id)


def write_conserved_interface_dict(outdir=None,pdb_id=None):
    assert pdb_id is not None
    if outdir is None:
        outdir = os.getcwd()
    cons_dict = {}
    intf_dict = {}
    for cs in os.listdir(outdir):
        if cs.endswith('_conserved_residues_dict.json'):
            cons_dict = os.path.join(outdir,cs)
        elif cs.endswith('_interface_residues_dict.json'):
            intf_dict = os.path.join(outdir,cs)
    if intf_dict and cons_dict:
        dict_conserved_interface_residues = {}
        with open(cons_dict,'rb') as cons:
            conserved_residues_dict = json.load(cons)
        with open(intf_dict,'rb') as intf:
            interface_residues_dict = json.load(intf)
    for ch_pair in interface_residues_dict.keys():
        ch1 = ch_pair.split('_')[0]
        ch2 = ch_pair.split('_')[-1]
        ch1_intf_res = interface_residues_dict[ch_pair][0]
        ch2_intf_res = interface_residues_dict[ch_pair][1]
        ch1_conserved_residues = []
        ch2_conserved_residues = []
        if (ch1 in conserved_residues_dict.keys()) and (ch2 in conserved_residues_dict.keys()):
            for res1 in conserved_residues_dict[ch1]:
                ch1_conserved_residues.append(res1[0])
            for res2 in conserved_residues_dict[ch2]:
                ch2_conserved_residues.append(res2[0])
            interface_conserved_residues_ch1 = list(set(ch1_intf_res) & set(ch1_conserved_residues))
            interface_conserved_residues_ch2 = list(set(ch2_intf_res) & set(ch2_conserved_residues))
            conserved_residues_at_interface = len(interface_conserved_residues_ch1) + len(interface_conserved_residues_ch2)
            total_interface_residues = len(ch1_intf_res) + len(ch2_intf_res)
            norm_conserved_interface_residues = float(conserved_residues_at_interface)/total_interface_residues
        else:
            #if conservation not calculated for that chain, then this value is 'NA'
            norm_conserved_interface_residues = 'NA'
        dict_conserved_interface_residues[ch_pair] = norm_conserved_interface_residues
    outdict = os.path.join(outdir,pdb_id + '_conserved_interface_dict.json')
    with open(outdict,'w') as outfle:
        json.dump(dict_conserved_interface_residues,outfle)
            
            
        
    
    
