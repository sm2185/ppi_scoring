import setup_names 
import os
import subprocess

def check_paths():
    #check valid paths
    path_names = ['muscle_path','usearch_path','db_path']
    not_set = []
    all_p = setup_names.__dict__
    fl = 0
    print 'Checking local paths'
    for na in all_p.keys():
        if not na.startswith('__'): 
            if na in path_names:
                if not os.path.isfile(all_p[na]):
                    not_set.append(na)
            else:  
                try:
                    child = subprocess.Popen(all_p[na],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    stdout = child.communicate()[0]
                    fl = child.returncode
                except OSError:
                    fl = 0
                if fl == 0:
                    not_set.append(na) 
    if not_set:
        return not_set
    else:
        return True
    
